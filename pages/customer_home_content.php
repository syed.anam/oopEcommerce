<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">Congratulation <?php echo $_SESSION['customer_name']; ?>.  Your order successfully submitted. Please wait we will contact with you soon...</h3>
                    </div>
                </div>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1 class="text-center text-success">My HOME</h1>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>