<?php
    if (isset($_POST['btn'])) {
        $obj_checkout->save_order_info($_POST);
    }
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">Dear <?php echo $_SESSION['customer_name']; ?>,  you have to give us product payment information to complete your checkout process.</h3>
                    </div>
                </div>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">Please select your payment method</h3>
                        <hr/>
                        <form class="form-horizontal" action="" method="post">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Cash On Delivary</th>
                                    <th><input type="radio" name="payment_type" value="cash_ondelivary"></th>
                                </tr>
                                <tr>
                                    <th>Bkash</th>
                                    <th><input type="radio" name="payment_type" value="bkash"></th>
                                </tr>
                                <tr>
                                    <th>Paypal</th>
                                    <th><input type="radio" name="payment_type" value="paypal"></th>
                                </tr>
                                <tr>
                                    <th colspan="2"><input type="submit" name="btn" value="Confirm Order" class="btn btn-primary btn-block"></th>
                                </tr>
                            </table>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>