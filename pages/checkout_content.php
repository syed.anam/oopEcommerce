<?php
if (isset($_POST['btn'])) {
    $obj_checkout->save_customer_info($_POST);
}
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">You have to login to complete your checkout process. If  you are not registered then please register first Thanks !</h3>
                    </div>
                </div>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">You may to login here</h3>
                        <hr/>
                        <form class="form-horizontal" action=" " method="post">
                            <div class="form-group">
                                <label class="control-label col-lg-3">Email Address</label>
                                <div class="col-lg-9">
                                    <input type="email" name="email_address" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Password</label>
                                <div class="col-lg-9">
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3"></label>
                                <div class="col-lg-9">
                                    <input type="submit" name="btn" class="btn btn-primary btn-block" value="Login">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">You may to Register here</h3>
                        <hr/>
                        <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                                <label class="control-label col-lg-3">First Name</label>
                                <div class="col-lg-9">
                                    <input type="text" name="first_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Last Name</label>
                                <div class="col-lg-9">
                                    <input type="text" name="last_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Email Address</label>
                                <div class="col-lg-9">
                                    <input type="email" name="email_address" id="email_address" class="form-control" onblur="ajax_email_check();
                                           ">
                                    <span id="res"></span>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Password</label>
                                <div class="col-lg-9">
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Phone Number</label>
                                <div class="col-lg-9">
                                    <input type="number" name="phone_number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Address</label>
                                <div class="col-lg-9">
                                    <textarea name="address" class="form-control" rows="8"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3"></label>
                                <div class="col-lg-9">
                                    <input type="submit" name="btn" id="btn" class="btn btn-primary btn-block" value="Sign Up">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function ajax_email_check() {
       var given_email = document.getElementById('email_address').value;
        var xmlHttp = new XMLHttpRequest();
        var server_page = 'email_check.php?email=' + given_email;
        xmlHttp.open('GET', server_page);
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                document.getElementById('res').innerHTML = xmlHttp.responseText;
                if(xmlHttp.responseText == 'Email address already exists') {
                    document.getElementById('btn').disabled = true;
                } else {
                    document.getElementById('btn').disabled = false;
                }
            }
        }
        xmlHttp.send(null);
    }
</script>
