<?php
   $customer_id = $_SESSION['customer_id'];
   $query_result = $obj_checkout->select_customer_info_by_id($customer_id);        
   $customer_info = mysqli_fetch_assoc($query_result);
   extract($customer_info);
   
    if(isset($_POST['btn'])) {
        $obj_checkout->save_shipping_info($_POST, $customer_id);
    }


?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">Dear <?php echo $_SESSION['customer_name']; ?>,  you have to give us product shipping information to complete your checkout process. If  your billing info and shipping info are same then just press on save shipping info button </h3>
                    </div>
                </div>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="text-center text-success">You may to Register here</h3>
                        <hr/>
                        <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                                <label class="control-label col-lg-3">Full Name</label>
                                <div class="col-lg-9">
                                    <input type="text" name="full_name" value="<?php echo $first_name. ' '.$last_name; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Email Address</label>
                                <div class="col-lg-9">
                                    <input type="email" name="email_address" value="<?php echo $email_address; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Phone Number</label>
                                <div class="col-lg-9">
                                    <input type="number" name="phone_number" value="<?php echo $phone_number; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Address</label>
                                <div class="col-lg-9">
                                    <textarea name="address" class="form-control" rows="8"><?php echo $address; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3"></label>
                                <div class="col-lg-9">
                                    <input type="submit" name="btn" class="btn btn-primary btn-block" value="Save Shipping Info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
