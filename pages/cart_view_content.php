<?php
$message = '';
    if(isset($_POST['btn'])) {
       $message = $obj_cart->update_cart_product($_POST);
    }
    

    $query_result = $obj_cart->select_cart_product_by_session_id();
?>
<div class="checkout">
    <div class="container">
        <h3>My Shopping Bag</h3>
        <h3><?php echo $message; ?></h3>
        <div class="table-responsive checkout-right animated wow slideInUp" data-wow-delay=".5s">
            <table class="timetable_sub">
                <thead>
                    <tr>
                        <th>Remove</th>
                        <th>Product Image</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <?php $sum = 0; while ($cart_product = mysqli_fetch_assoc($query_result)) { ?>
                <tr class="rem1">
                    <td class="invert-closeb">
                        <div class="rem">
                            <a href="?status=delete&&id=<?php echo $cart_product['product_id']; ?>" class="close1"></a>
                        </div>
                    </td>
                    <td class="invert-image"><a href="single.html"><img src="images/<?php echo $cart_product['product_image']; ?>" alt=" " class="img-responsive" /></a></td>
                    <td class="invert"><?php echo $cart_product['product_name']; ?></td>
                    <td class="invert"> BDT <?php echo $cart_product['product_price']; ?></td>
                    <td class="invert">
                        <div class="quantity"> 
                            <div class="quantity-select">                           
                                <form action=" " method="post">
                                    <input type="number" min="1" name="product_quantity" value="<?php echo $cart_product['product_quantity']; ?>" >
                                    <input type="hidden" name="product_id" value="<?php echo $cart_product['product_id']; ?>" >
                                    <input type="submit" name="btn" value="Update" >
                                </form>
                            </div>
                        </div>
                    </td>
                    
                    <td class="invert">
                        <?php
                            $item_total = $cart_product['product_price']* $cart_product['product_quantity'];
                            echo 'BDT'.' '.$item_total;
                        ?>
                    </td>
                </tr>
                <?php $sum = $sum + $item_total; } ?>
                <!--quantity-->
                <script>
                    $('.value-plus').on('click', function () {
                        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
                        divUpd.text(newVal);
                    });

                    $('.value-minus').on('click', function () {
                        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
                        if (newVal >= 1)
                            divUpd.text(newVal);
                    });
                </script>
                <!--quantity-->
            </table>
        </div>
        <div class="checkout-left">	

            <div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
                <a href="index.php"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back To Shopping</a>
                <?php  if(isset($_SESSION['customer_id']) && isset($_SESSION['shipping_id'])) {   ?>
                <a href="payment.php"> Checkout &nbsp; &nbsp;<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </a>
                    <?php } else if (isset($_SESSION['customer_id'])) {  ?>
                <a href="shipping.php"> Checkout &nbsp; &nbsp;<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </a>
                    <?php } else { ?>
                <a href="checkout.php"> Checkout &nbsp; &nbsp;<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> </a>
                    <?php } ?>
            </div>
            <div class="checkout-left-basket animated wow slideInLeft" data-wow-delay=".5s">
                <h4>Shopping basket</h4>
                <ul>
                    <li>Sub Total <i>-</i> <span>BDT <?php echo $sum; ?></span></li>
                    <li>Vat Total <i>-</i> <span>
                        <?php
                            $vat = ($sum*15)/100;
                            echo 'BDT'.' '.$vat;
                        ?>
                        </span></li>
                    <li>Shipping Cost(inside Dhaka) <i>-</i> <span>Free</span></li>
                    <li>Grand Total <i>-</i> <span>
                        <?php
                            $grand_total=$sum+$vat;
                           echo 'BDT'.' '.$grand_total;
                           $_SESSION['order_total']=$grand_total;
                        ?>
                        </span></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>	