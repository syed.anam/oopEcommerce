<?php

require_once 'db_connect.php';

class product extends Db_Connect {

    //put your code here
    protected $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_product_info($data) {
        $directory = '../assets/product_image/';
        $target_file = $directory . $_FILES['product_image']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['product_image']['size'];
        $check = getimagesize($_FILES['product_image']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                die('This image already exist');
            } else {
                if ($file_size > 500000) {
                    die('File size is too large');
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        die('File type should be jpg or png');
                    } else {
                        move_uploaded_file($_FILES['product_image']['tmp_name'], $target_file);
                        $sql = "INSERT INTO tbl_product (product_name, category_id, manufacturer_id, product_price, product_quantity, product_short_description, product_long_description, product_image, publication_status) VALUES ('$data[product_name]', '$data[category_id]', '$data[manufacturer_id]', '$data[product_price]', '$data[product_quantity]', '$data[product_short_description]', '$data[product_long_description]', '$target_file',  '$data[publication_status]')";
                        if (mysqli_query($this->link, $sql)) {
                            $message = "Product info save successfully";
                            return $message;
                        } else {
                            die('Query problem' . mysqli_error($this->link));
                        }
                    }
                }
            }
        } else {
            die('This is not an image. please use a valid image');
        }
    }

    public function select_all_product_info() {
        $sql = "SELECT p.product_id, p.product_name, p.category_id, p.manufacturer_id, p.product_price, p.publication_status, c.category_name, m.manufacturer_name FROM tbl_product as p, tbl_category as c, tbl_manufacturer as m WHERE p.category_id = c. category_id AND p.manufacturer_id = m.manufacturer_id";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function select_product_info_by_id($product_id) {
        $sql = "SELECT p.*, c.category_name, m.manufacturer_name FROM tbl_product as p, tbl_category as c, tbl_manufacturer as m WHERE p.category_id = c. category_id AND p.manufacturer_id = m.manufacturer_id AND p.product_id = '$product_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }

    public function product_publication_status_change_by_id($product_id) {
        if ($_GET['status'] == "publish") {
            $sql = "UPDATE tbl_product SET publication_status=1 WHERE product_id='$product_id' ";
            if (mysqli_query($this->link, $sql)) {
                $message = "Product info Published Successfully";
                return $message;
            } else {
                die("Query Problem" . mysqli_error($this->link));
            }
        } elseif ($_GET['status'] == "unpublish") {

            $sql = "UPDATE tbl_product SET publication_status = 0 WHERE product_id='$product_id' ";
            if (mysqli_query($this->link, $sql)) {
                $message = "Product info Unpublished Successfully";
                return $message;
            } else {
                die("Query Problem" . mysqli_error($this->link));
            }
        }
    }

    public function update_product_info($data) {
        extract($data);
        $new_image = $_FILES['product_image']['name'];
        if ($new_image) {
            $directory = '../assets/product_image/';
            $target_file = $directory . $_FILES['product_image']['name'];
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            $file_size = $_FILES['product_image']['size'];
            $check = getimagesize($_FILES['product_image']['tmp_name']);
            if ($check) {
                if (file_exists($target_file)) {
                    die('This image already exist');
                } else {
                    if ($file_size > 500000) {
                        die('File size is too large');
                    } else {
                        if ($file_type != 'jpg' && $file_type != 'png') {
                            die('File type should be jpg or png');
                        } else {
                            move_uploaded_file($_FILES['product_image']['tmp_name'], $target_file);
                            $sql = "UPDATE tbl_product SET product_name = '$product_name', category_id = '$category_id', manufacturer_id = '$manufacturer_id', product_price = '$product_price', product_quantity = '$product_quantity', product_short_description = '$product_short_description', product_long_description = '$product_long_description', product_image = '$target_file', publication_status = '$publication_status' WHERE product_id = '$data[product_id]' ";
                            if (mysqli_query($this->link, $sql)) {
                                $_SESSION['message'] = "Product info update successfully";
                                header('Location: manage_product.php');
                            } else {
                                die('Query problem' . mysqli_error($this->link));
                            }
                        }
                    }
                }
            } else {
                die('This is not an image. please use a valid image');
            }
        } else {
            $sql = "UPDATE tbl_product SET product_name = '$product_name', category_id = '$category_id', manufacturer_id = '$manufacturer_id', product_price = '$product_price', product_quantity = '$product_quantity', product_short_description = '$product_short_description', product_long_description = '$product_long_description', publication_status = '$publication_status' WHERE product_id = '$data[product_id]' ";
            if (mysqli_query($this->link, $sql)) {
                $_SESSION['message'] = "Product info update successfully";
                header('Location: manage_product.php');
            } else {
                die('Query problem' . mysqli_error($this->link));
            }
        }
    }
    public function select_all_order_info() {
        $sql = "SELECT o.order_id, o.order_total, o.order_status, c.first_name, c.last_name, p.payment_type, p.payment_status FROM tbl_order as o, tbl_customer as c, tbl_payment as p WHERE o.customer_id = c.customer_id AND o.order_id = p.order_id ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_customer_info_by_order_id($order_id) {
        $sql = "SELECT o.customer_id, c.* FROM tbl_order as o, tbl_customer as c WHERE o.customer_id = c.customer_id AND o.order_id = '$order_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_shipping_info_by_order_id($order_id) {
        $sql = "SELECT o.shipping_id, s.* FROM tbl_order as o, tbl_shipping as s WHERE o.shipping_id = s.shipping_id AND o.order_id = '$order_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    public function select_product_info_by_order_id($order_id) {
        $sql = "SELECT o.order_id, od.* FROM tbl_order as o, tbl_order_details as od WHERE o.order_id = od.order_id AND o.order_id = '$order_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->link));
        }
    }
    

}
