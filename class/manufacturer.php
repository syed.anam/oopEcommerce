<?php

require_once 'db_connect.php';

class Manufacturer extends Db_connect {

    protected $link;

    public function __construct() {
        $this->link = $this->database_connection();
    }

    public function save_manufacturer_info($data) {
        extract($data);
        $sql = "INSERT INTO tbl_manufacturer (manufacturer_name,manufacturer_description,publication_status) VALUES ('$manufacturer_name','$manufacturer_description','$publication_status')";
        if (mysqli_query($this->link, $sql)) {
            $_SESSION['message'] = "Save Manufacturer info Successfully";
//      $message="Save Manufacturer info Successfully";
//      return $message;
        } else {
            die("Query Problem" . mysqli_error($this->link));
        }
    }

    public function select_all_manufacturer_info() {
        $sql = "SELECT * FROM tbl_manufacturer WHERE deletion_status=1";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die("Query Problem" . mysqli_error($this->link));
        }
    }

    public function select_all_manufacturer_info_by_id($manufacturer_id) {
        $sql = "SELECT * FROM tbl_manufacturer WHERE manufacturer_id='$manufacturer_id' ";
        if (mysqli_query($this->link, $sql)) {
            $query_result = mysqli_query($this->link, $sql);
            return $query_result;
        } else {
            die("Query Problem" . mysqli_error($this->link));
        }
    }

    public function update_manufacturer_info_by_id($data) {
        extract($data);
        $sql = "UPDATE tbl_manufacturer SET manufacturer_name='$manufacturer_name',manufacturer_description='$manufacturer_description',publication_status='$publication_status' WHERE manufacturer_id='$manufacturer_id' ";
        if (mysqli_query($this->link, $sql)) {
            $_SESSION['message'] = "Manufacturer info Update Successfully";

            return $message;
        } else {
            die("Query Problem" . mysqli_error($this->link));
        }
    }

    public function delete_manufacturer_info_by_id($manufacturer_id) {
        $sql = "UPDATE tbl_manufacturer SET deletion_status=0 WHERE manufacturer_id='$manufacturer_id' ";
        if (mysqli_query($this->link, $sql)) {
            $message = "Manufacturer info Delete Successfully";
            return $message;
        } else {
            die("Query Problem" . mysqli_error($this->link));
        }
    }

    public function manufacturer_publication_status_change_by_id($manufacturer_id) {
        if ($_GET['status'] == "publish") {
            $sql = "UPDATE tbl_manufacturer SET publication_status=0 WHERE manufacturer_id='$manufacturer_id' ";
            if (mysqli_query($this->link, $sql)) {
                $message = "Manufacturer info Unpublished Successfully";
                return $message;
            } else {
                die("Query Problem" . mysqli_error($this->link));
            }
        } elseif ($_GET['status'] == "unpublish") {
            $sql = "UPDATE tbl_manufacturer SET publication_status=1 WHERE manufacturer_id='$manufacturer_id' ";
            if (mysqli_query($this->link, $sql)) {
                $message = "Manufacturer info Published Successfully";
                return $message;
            } else {
                die("Query Problem" . mysqli_error($this->link));
            }
        }
    }

}
