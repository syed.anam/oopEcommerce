<?php
$message = '';
if (isset($_GET['status'])) {
    $product_id = $_GET['id'];
    if ($_GET['status'] == "delete") {
        $message = $obj_product->delete_product_info_by_id($product_id);
    } elseif ($_GET['status'] == "publish" || $_GET['status'] == "unpublish") {
        $message = $obj_product->product_publication_status_change_by_id($product_id);
    }
}

$query_result = $obj_product->select_all_order_info();
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="lead panel-heading">
                All Order Information Goes Here 
                <h3 class="text-primary">
                    <?PHP
                    echo $message;


                    if (isset($_SESSION['message'])) {
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                    }
                    ?>
                </h3>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Order ID</th>
                            <th>Customer Name</th>
                            <th>Order Total</th>
                            <th>Order Status</th>
                            <th>Payment Type</th>
                            <th>Payment Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($order_info = mysqli_fetch_assoc($query_result)) {
                            extract($order_info);
                            ?>
                            <tr lass="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $order_id; ?></td>
                                <td><?php echo $first_name . ' ' . $last_name; ?></td>
                                <td><?php echo $order_total; ?></td>
                                <td><?php echo $order_status; ?></td>
                                <td><?php echo $payment_type; ?></td>
                                <td><?php echo $payment_status; ?></td>
                                <td class="center">
                                    <a href="view_order.php?id=<?php echo $order_id; ?>" class="btn btn-info" title="View Order Details">  
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                    <a href="view_invoice.php?id=<?php echo $order_id; ?>" class="btn btn-primary" title="View Invoice">  
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                    <a href="?status=unpublish&&id=<?php echo $order_id; ?>" class="btn btn-success" title="Download Invoice"> 
                                        <span class="glyphicon glyphicon-download"></span>
                                    </a>
                                    <a href="edit_product.php?id=<?php echo $order_id; ?>" class="btn btn-default" title="Edit Order">  
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $order_id; ?>" class="btn btn-danger" title="Delete Order" onclick="return check_delete_status();">  
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function check_delete_status() {
        var check = confirm("Are you sure to Delete This !! ");
        if (check) {
            return true;
        } else {
            return false;
        }
    }
</script>