<?php
require '../class/application.php';
$obj_app = new Application();

$query_result = $obj_app->select_all_published_category_info();
$query_result1 = $obj_app->select_all_published_manufacturer_info();

$message='';
if(isset($_POST['btn'])) {
    $message = $obj_product->save_product_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <p class="lead text-success text-center">Add Product Form</p>
            <h3 class="text-success text-center"><?php echo $message; ?></h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Name</label>
                    <div class="col-lg-9">
                        <input type="text" name="product_name" required class="form-control">
                    </div>
                </div>
                 <div class="form-group">
                    <label class="control-label col-lg-3">Category Name</label>
                    <div class="col-lg-9">
                        <select name="category_id" class="form-control">
                            <option>---Select Product Category---</option>
                           <?php while ($category_info = mysqli_fetch_assoc($query_result)) { ?>
                            <option value="<?php echo $category_info['category_id']; ?>"><?php echo $category_info['category_name']; ?></option>
                           <?php } ?>
                        </select>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="control-label col-lg-3">Manufacturer Name</label>
                    <div class="col-lg-9">
                        <select name="manufacturer_id" class="form-control">
                            <option>---Select Product Manufacturer---</option>
                            <?php while ($manufacturer_info = mysqli_fetch_assoc($query_result1)) { ?>
                            <option value="<?php echo $manufacturer_info['manufacturer_id']; ?>"><?php echo $manufacturer_info['manufacturer_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Price</label>
                    <div class="col-lg-9">
                        <input type="number" name="product_price" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Quantity</label>
                    <div class="col-lg-9">
                        <input type="number" name="product_quantity" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Short Description</label>
                    <div class="col-lg-9">
                        <textarea name="product_short_description" class="form-control" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Long Description</label>
                    <div class="col-lg-9">
                        <textarea name="product_long_description" class="form-control" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Product Image</label>
                    <div class="col-lg-9">
                        <input type="file" name="product_image" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Publication Status</label>
                    <div class="col-lg-9">
                        <select name="publication_status" class="form-control">
                            <option>---Select Publication Status---</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3"></label>
                    <div class="col-lg-9">
                        <input type="submit" name="btn" value="Create Product" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>