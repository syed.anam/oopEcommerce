<?php
//$message='';
if (isset($_POST['btn'])) {
    $message=$obj_manufacturer->update_manufacturer_info_by_id($_POST);
    header("Location:manage_manufacturer.php");
}

$manufacturer_id = $_GET['id'];
$query_result = $obj_manufacturer->select_all_manufacturer_info_by_id($manufacturer_id);
$query_info = mysqli_fetch_assoc($query_result);
extract($query_info);
?>
<div class="row">
  <div class="col-lg-12">
    <div class="well well-sm">
      <p class="lead text-success text-center">Update Manufacturer Form</p>
      <h3 class="text-success text-center"><?php // echo $message; ?></h3>
    </div>
  </div>    
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="well well-sm">
      <form class="form-horizontal" action="" method="post" name="manufacturer_update_form">

        <div class="form-group">
          <label class="control-label col-lg-3">Manufacturer Name</label>
          <div class="col-lg-9">
            <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>"  class="form-control"/>
            <input type="text" name="manufacturer_name" value="<?php echo $manufacturer_name; ?>" required class="form-control"/>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Manufacturer Description</label>
          <div class="col-lg-9">
            <textarea name="manufacturer_description" class="form-control" rows="10"><?php echo $manufacturer_description; ?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-lg-3">Publication Status</label>
          <div class="col-lg-9">
            <select name="publication_status" class="form-control">
              <option>Select Publication status</option>
              <option value="1">Published</option>
              <option value="0">Unpublished</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-lg-3"></label>
          <div class="col-lg-9">
            <input type="submit" name="btn" value="Create Manufacturer" class="btn btn-primary btn-block"/>                        
          </div>
        </div>                
      </form>
    </div>
  </div>    
</div>
<script>
  document.forms['manufacturer_update_form'].elements['publication_status'].value="<?php echo $publication_status; ?>";  
</script>

