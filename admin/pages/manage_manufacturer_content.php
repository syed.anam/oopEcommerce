<?php
$query_result = $obj_manufacturer->select_all_manufacturer_info();
$message='';

if (isset($_GET['status'])){
  $manufacturer_id=$_GET['id'];
  if ($_GET['status']=="delete"){
   $message=$obj_manufacturer->delete_manufacturer_info_by_id($manufacturer_id);
   header("Location:manage_manufacturer.php");   
  }
  elseif ($_GET['status']=="publish") {
   $message=$obj_manufacturer->manufacturer_publication_status_change_by_id($manufacturer_id);
   header("Location:manage_manufacturer.php"); 
    
  }
  elseif ($_GET['status']=="unpublish") {
   $message=$obj_manufacturer->manufacturer_publication_status_change_by_id($manufacturer_id);
   header("Location:manage_manufacturer.php"); 
   
  }
}
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="lead panel-heading">
        All Manufacturer Information Goes Here 
        <h3 class="text-primary">
        <?PHP
        echo $message;

        
            if(isset($_SESSION['message'])){
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h3>
      </div>
      <div class="panel-body">
        <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
          <thead>
            <tr>
              <th>Manufacturer Name</th>
              <th>Manufacturer Description</th>
              <th>Publication Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while ($query_info = mysqli_fetch_assoc($query_result)) {
              extract($query_info);
              ?>
              <tr lass="odd gradeX">
                <td><?php echo $manufacturer_name; ?></td>
                <td><?php echo $manufacturer_description; ?></td>
                <td>
                  <?php
                  if ($publication_status == 1) {
                    echo "Published";
                  } else {
                    echo 'Unpublished';
                  };
                  ?>
                </td>
                <td class="center">
                  <?php if ($publication_status == 1) { ;?>
                  <a href="?status=publish&&id=<?php echo $manufacturer_id; ?>" class="btn btn-xs btn-success" title="Publish"> 
                    <span class="glyphicon glyphicon-arrow-up"></span>
                  </a>
                  <?php }  else { ;?>
                  <a href="?status=unpublish&&id=<?php echo $manufacturer_id; ?>" class="btn btn-xs btn-danger" title="UnPublish">  
                    <span class="glyphicon glyphicon-arrow-down"></span>
                  </a>
                  <?php } ;?>
                  <a href="edit_manufacturer.php?id=<?php echo $manufacturer_id; ?>" class="btn btn-xs btn-info" title="Edit">  
                    <span class="glyphicon glyphicon-edit"></span>
                  </a>
                  <a href="?status=delete&&id=<?php echo $manufacturer_id; ?>" class="btn btn-xs btn-danger" title="Delete" onclick="return check_delete_status();">  
                    <span class="glyphicon glyphicon-trash"></span>
                  </a>
                  
                </td>
              </tr>
            <?php }; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  function check_delete_status(){
    var check = confirm("Are you sure to Delete This !! ");
    if(check){
      return true;
    }else{
      return false;
    }
  }
  </script>