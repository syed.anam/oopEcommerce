<?php
$message='';
if(isset($_POST['btn'])){    
    $message=$obj_manufacturer->save_manufacturer_info($_POST);
    header("Location:manage_manufacturer.php");
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <p class="lead text-success text-center">Add Manufacturer Form</p>
            <h3 class="text-success text-center"><?php // echo $message;?></h3>
        </div>
    </div>    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <form class="form-horizontal" action="" method="post">
                
                <div class="form-group">
                    <label class="control-label col-lg-3">Manufacturer Name</label>
                    <div class="col-lg-9">
                        <input type="text" name="manufacturer_name" required class="form-control"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-lg-3">Manufacturer Description</label>
                    <div class="col-lg-9">
                        <textarea name="manufacturer_description" class="form-control" rows="10"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-lg-3">Publication Status</label>
                    <div class="col-lg-9">
                        <select name="publication_status" class="form-control">
                            <option>Select Publication status</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3"></label>
                    <div class="col-lg-9">
                        <input type="submit" name="btn" value="Create Manufacturer" class="btn btn-primary btn-block"/>                        
                    </div>
                </div>                
            </form>
        </div>
    </div>    
</div>

