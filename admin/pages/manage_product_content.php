<?php
$message = '';
if (isset($_GET['status'])) {
    $product_id = $_GET['id'];
    if ($_GET['status'] == "delete") {
        $message = $obj_product->delete_product_info_by_id($product_id);
    } elseif ($_GET['status'] == "publish" || $_GET['status'] == "unpublish") {
        $message = $obj_product->product_publication_status_change_by_id($product_id);
    }
}

$query_result = $obj_product->select_all_product_info();
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="lead panel-heading">
                All Product Information Goes Here 
                <h3 class="text-primary">
                    <?PHP
                    echo $message;


                    if (isset($_SESSION['message'])) {
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                    }
                    ?>
                </h3>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SL NO</th>
                            <th>Product Name</th>
                            <th>Category Name</th>
                            <th>Manufacturer Name</th>
                            <th>Product Price</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($product_info = mysqli_fetch_assoc($query_result)) {
                            extract($product_info);
                            ?>
                            <tr lass="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $product_name; ?></td>
                                <td><?php echo $category_name; ?></td>
                                <td><?php echo $manufacturer_name; ?></td>
                                <td><?php echo $product_price; ?></td>
                                <td>
                                    <?php
                                    if ($publication_status == 1) {
                                        echo "Published";
                                    } else {
                                        echo 'Unpublished';
                                    };
                                    ?>
                                </td>
                                <td class="center">
                                    <a href="view_product.php?id=<?php echo $product_id; ?>" class="btn btn-xs btn-info" title="View Product">  
                                        <span class="glyphicon glyphicon-zoom-in"></span>
                                    </a>
                                    <?php if ($publication_status == 1) { ?>
                                        <a href="?status=unpublish&&id=<?php echo $product_id; ?>" class="btn btn-xs btn-success" title="Unpublish Product"> 
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="?status=publish&&id=<?php echo $product_id; ?>" class="btn btn-xs btn-danger" title="Publish Product ">  
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    <?php } ?>
                                    <a href="edit_product.php?id=<?php echo $product_id; ?>" class="btn btn-xs btn-info" title="Edit Product">  
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $product_id; ?>" class="btn btn-xs btn-danger" title="Delete Product" onclick="return check_delete_status();">  
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++;   }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function check_delete_status() {
        var check = confirm("Are you sure to Delete This !! ");
        if (check) {
            return true;
        } else {
            return false;
        }
    }
</script>