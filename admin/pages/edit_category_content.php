<?php
$category_id = $_GET['id'];
$query_result = $obj_category->select_category_info_by_id($category_id);
$category_info = mysqli_fetch_assoc($query_result);
extract($category_info);

if(isset($_POST['btn'])) {
    $message = $obj_category->update_category_info($_POST);
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <p class="lead text-success text-center">Edit Category Form</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="well well-sm">
            <form class="form-horizontal" action="" method="post" name="edit_category_form">
                <div class="form-group">
                    <label class="control-label col-lg-3">Category Name</label>
                    <div class="col-lg-9">
                        <input type="text" name="category_name" value="<?php echo $category_name; ?>" required class="form-control">
                        <input type="hidden" name="category_id" value="<?php echo $category_id; ?>" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Category Description</label>
                    <div class="col-lg-9">
                        <textarea name="category_description" class="form-control" rows="10"><?php echo $category_description; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Publication Status</label>
                    <div class="col-lg-9">
                        <select name="publication_status" class="form-control">
                            <option>---Select Publication Status---</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3"></label>
                    <div class="col-lg-9">
                        <input type="submit" name="btn" value="Update Category" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    document.forms['edit_category_form'].elements['publication_status'].value = '<?php echo $publication_status; ?>';  
</script>