<?php
$order_id = $_GET['id'];
$billing_query_result = $obj_product->select_customer_info_by_order_id($order_id);
$billing_info = mysqli_fetch_assoc($billing_query_result);
extract($billing_info);
$delivery_query_result = $obj_product->select_shipping_info_by_order_id($order_id);
$delivery_info = mysqli_fetch_assoc($delivery_query_result);
//extract($delivery_info);
$product_query_result = $obj_product->select_product_info_by_order_id($order_id);
?>
<style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }

    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }

    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }

    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }

    .invoice-box table tr.details td{
        padding-bottom:20px;
    }

    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }

    .invoice-box table tr.item.last td{
        border-bottom:none;
    }

    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }

        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
</style>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="http://nextstepwebs.com/images/logo.png" style="width:100%; max-width:300px;">
                        </td>
                        <td>
                            Invoice #: 00<?php echo $order_id; ?><br>
                            Created: <?php echo date('Y-M-l') ?><br>
                            Due: February 1, 2015
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <h3>Billing Info</h3>
                            <hr/>
                            <span>Name : </span> <?php echo $first_name . ' ' . $last_name; ?><br>
                            <span>Phone Number : </span> <?php echo $phone_number; ?><br>
                            <span>Address : </span> <?php echo $address; ?>
                        </td>
                        <td>
                            <h3>Delivery Info</h3>
                            <hr/>
                            <span>Name : </span> <?php echo $delivery_info['full_name']; ?><br>
                            <span>Phone Number : </span> <?php echo $delivery_info['phone_number']; ?><br>
                            <span>Address : </span> <?php echo $delivery_info['address']; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Payment Method
            </td>

            <td>
                Check #
            </td>
        </tr>

        <tr class="details">
            <td>
                Check
            </td>

            <td>
                1000
            </td>
        </tr>

        <tr class="heading">
            <td> Item </td>
            <td> Quantity </td>
            <td>Price </td>
        </tr>
        <?php $sum = 0;
        while ($invoice_product = mysqli_fetch_assoc($product_query_result)) { ?>
            <tr class="item">
                <td><?php echo $invoice_product['product_name']; ?></td>
                <td><?php echo $invoice_product['product_quantity']; ?></td>
                <td>BDT <?php
                    $item_total = $invoice_product['product_price'];
                    echo $item_total;
                    ?></td>
            </tr>
    <?php $sum = $sum + $item_total;
} ?>
        <tr class="total">
            <td></td>

            <td>
                Total: BDT <?php echo $sum; ?>
            </td>
        </tr>
    </table>
</div>
