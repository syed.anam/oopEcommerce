<?php
$order_id = $_GET['id'];
$customer_query_result = $obj_product->select_customer_info_by_order_id($order_id);
$customer_info =mysqli_fetch_assoc($customer_query_result);
extract($customer_info);
$shipping_query_result = $obj_product->select_shipping_info_by_order_id($order_id);
$shipping_info =mysqli_fetch_assoc($shipping_query_result);
extract($shipping_info);
$product_query_result = $obj_product->select_product_info_by_order_id($order_id);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="lead panel-heading">
                 Order Information Goes Here 
            </div>
            <div class="panel-body">
                <h3 class="text-center">Customer Information for this order</h3>
                <hr/>
                <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
                        <tr>
                            <th>Customer Name</th>
                            <td><?php echo $first_name.' '.$last_name; ?></td>
                        </tr>
                        <tr>
                            <th>Email Address</th>
                            <td><?php echo $email_address; ?></td>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <td><?php echo $phone_number; ?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td><?php echo $address; ?></td>
                        </tr>
                </table>
                <h3 class="text-center">Shipping Information for this order</h3>
                <hr/>
                <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
                        <tr>
                            <th>Customer Name</th>
                            <td><?php echo $full_name; ?></td>
                        </tr>
                        <tr>
                            <th>Email Address</th>
                            <td><?php echo $email_address; ?></td>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <td><?php echo $phone_number; ?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td><?php echo $address; ?></td>
                        </tr>
                </table>
                <h3 class="text-center">Product Information for this order</h3>
                <hr/>
                <table width="100%" class="table table-bordered table-responsive table-striped table-hover" id="dataTables-example">
                        <tr>
                            <th>Product Name</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                            <th>Total Price</th>
                        </tr>
                        <?php while ($order_product = mysqli_fetch_assoc($product_query_result)) { ?>
                        <tr>
                            <td>Product Name</td>
                            <td>Product Name</td>
                            <td>Product Name</td>
                            <td>Product Name</td>
                        </tr>
                        <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>